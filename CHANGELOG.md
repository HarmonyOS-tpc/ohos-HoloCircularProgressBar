1.0.0

Initial Release



Support Features:

--------------------------------------------
1) 设置是否显示进度条的标记点的标记

         mHoloCircularProgressBar.setMarkerEnabled();
1) 设置标记点的位置
        
        mHoloCircularProgressBar.setMarkerProgress();
1) 设置进度的位置

        mHoloCircularProgressBar.setProgress();
1) 设置进度条的背景颜色
        
        mHoloCircularProgressBar.setProgressBackgroundColor();
1) 设置进度条的颜色
        
        mHoloCircularProgressBar.setProgressColor();
1) 设置是否显示进度条的当前进度的标记
        
        mHoloCircularProgressBar.setThumbEnabled();
1) 设置进度条的宽度
        
        mHoloCircularProgressBar.setWheelSize();

1) 获取进度条的宽度
        
        mHoloCircularProgressBar.getCircleStrokeWidth();
1) 获取标记点的位置
        
        mHoloCircularProgressBar.getMarkerProgress();
1) 获取当前的进度
        
        mHoloCircularProgressBar.getProgress();
1) 获取进度条的颜色
        
        mHoloCircularProgressBar.getProgressColor();
1) 获取是否可以标记点
        
        mHoloCircularProgressBar.isMarkerEnabled();
1) 获取是否可以显示当前进度的标记
        
        mHoloCircularProgressBar.isThumbEnabled();



Not Support Features:

--------------------------------------------------------

无