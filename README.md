# harmony-HoloCircularProgressBar

圆形的自定义ProgressBar

项目移植状态：支持组件所有基本功能

完成度：100%

调用差异：无





**导入方法**

1.har导入

将har包放入lib文件夹并在build.gradle添加

```
implementation fileTree(dir: 'libs', include: ['*.har'])
```

2.Library引用

添加本工程中UcropLib模块到任意工程中，在需要使用的模块build.gradle中添加

```
implementation project(':library')
```

or

```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:ohos-HoloCircularProgressBar:1.0.2'
```


**使用方法**
1 .在布局文件中添加以下代码
```
<HoloCircularProgressBar
        ohos:id="$+id:hcp_main"
        ohos:height="300vp"
        ohos:width="300vp"
        ohos:background_element="#33000000"
        ohos:below="$id:auto_animate_switch"
        ohos:holo_progress="0.4"
        ohos:horizontal_center="true"
        ohos:marker_progress="0.3"
/>
```
2.在代码中初始化参数
```
//初始化控件
mHoloCircularProgressBar = (HoloCircularProgressBar) findComponentById(ResourceTable.Id_hcp_main);
//设置标记点
mHoloCircularProgressBar.setMarkerProgress(0f);
//设置当前进度点
progressBar.setProgress(0.5f);
```
3.一些功能设置介绍
```
//设置是否显示进度条的标记点的标记
mHoloCircularProgressBar.setMarkerEnabled();
//设置标记点的位置
mHoloCircularProgressBar.setMarkerProgress();
//设置进度的位置
mHoloCircularProgressBar.setProgress();
//设置进度条的背景颜色
mHoloCircularProgressBar.setProgressBackgroundColor();
//设置进度条的颜色
mHoloCircularProgressBar.setProgressColor();
//设置是否显示进度条的当前进度的标记
mHoloCircularProgressBar.setThumbEnabled();
//设置进度条的宽度
mHoloCircularProgressBar.setWheelSize();

//获取进度条的宽度
mHoloCircularProgressBar.getCircleStrokeWidth();
//获取标记点的位置
mHoloCircularProgressBar.getMarkerProgress();
//获取当前的进度
mHoloCircularProgressBar.getProgress();
//获取进度条的颜色
mHoloCircularProgressBar.getProgressColor();
//获取是否可以标记点
mHoloCircularProgressBar.isMarkerEnabled();
//获取是否可以显示当前进度的标记
mHoloCircularProgressBar.isThumbEnabled();
mHoloCircularProgressBar
```
    
License
--
```Copyright 2013 Pascal Welsch
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
      http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.```
