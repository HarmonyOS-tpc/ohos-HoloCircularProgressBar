/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pascalwelsch.holocircularprogressbar.slice;

import com.pascalwelsch.holocircularprogressbar.ResourceTable;
import com.pascalwelsch.holocircularprogressbar.HoloCircularProgressBar;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AbsButton;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Switch;
import ohos.agp.utils.Color;

import java.security.SecureRandom;

/**
 * Main ability slice
 */
public class MainAbilitySlice extends AbilitySlice {
    /**
     * The constant M holo circular progress bar
     */
    private HoloCircularProgressBar mHoloCircularProgressBar;
    /**
     * The constant M animation has ended
     */
    protected boolean mAnimationHasEnded = false;

    /**
     * The constant M auto animate switch
     */
    private Switch mAutoAnimateSwitch;

    /**
     * The Switch button.
     */
    private Button mColorSwitchButton;


    /**
     * The constant M one
     */
    private Button mOne;

    /**
     * The constant M progress bar animator
     */
    private AnimatorValue mProgressBarAnimator;

    /**
     * The constant M zero
     */
    private Button mZero;

    /**
     * On start *
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mHoloCircularProgressBar = (HoloCircularProgressBar) findComponentById(ResourceTable.Id_hcp_main);
        mColorSwitchButton = (Button) findComponentById(ResourceTable.Id_random_color);
        mColorSwitchButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                switchColor();
            }
        });

        mZero = (Button) findComponentById(ResourceTable.Id_zero);
        mZero.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (mProgressBarAnimator != null) {
                    mProgressBarAnimator.cancel();
                }
                animate(mHoloCircularProgressBar, null, 0f, 1000);
                mHoloCircularProgressBar.setMarkerProgress(0f);
            }
        });

        mOne = (Button) findComponentById(ResourceTable.Id_one);
        mOne.setClickedListener(new Component.ClickedListener() {

            @Override
            public void onClick(Component component) {
                if (mProgressBarAnimator != null) {
                    mProgressBarAnimator.cancel();
                }
                animate(mHoloCircularProgressBar, null, 1f, 1000);
                mHoloCircularProgressBar.setMarkerProgress(1f);

            }
        });

        mAutoAnimateSwitch = (Switch) findComponentById(ResourceTable.Id_auto_animate_switch);
        mAutoAnimateSwitch.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                if (isChecked) {
                    mOne.setEnabled(false);
                    mZero.setEnabled(false);
                    animate(mHoloCircularProgressBar, stateChangedListener);
                } else {
                    mAnimationHasEnded = true;
                    mProgressBarAnimator.cancel();

                    mOne.setEnabled(true);
                    mZero.setEnabled(true);
                }

            }
        });
    }

    Animator.StateChangedListener stateChangedListener = new Animator.StateChangedListener() {

        @Override
        public void onStart(Animator animator) {

        }

        @Override
        public void onStop(Animator animator) {

        }

        @Override
        public void onCancel(Animator animator) {
            onEnd(animator);
        }

        @Override
        public void onEnd(Animator animator) {
            if (!mAnimationHasEnded) {
                animate(mHoloCircularProgressBar, this);
            } else {
                mAnimationHasEnded = false;
            }
        }

        @Override
        public void onPause(Animator animator) {

        }

        @Override
        public void onResume(Animator animator) {

        }
    };

    /**
     * Animate.
     *
     * @param progressBar the progress bar
     * @param listener    the listener
     */
    private void animate(final HoloCircularProgressBar progressBar, final Animator.StateChangedListener listener) {
        final float progress = (float) (Math.random() * 2);
        int duration = 3000;
        animate(progressBar, listener, progress, duration);
    }

    /**
     * Animate *
     *
     * @param progressBar progress bar
     * @param listener    listener
     * @param progress    progress
     * @param duration    duration
     */
    private void animate(final HoloCircularProgressBar progressBar, final Animator.StateChangedListener listener,
                final float progress, final int duration) {
        float progress1 = progressBar.getProgress();
        float sumx = progress - progress1;
        mProgressBarAnimator = new AnimatorValue();
        mProgressBarAnimator.setDuration(duration);
        mProgressBarAnimator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        mProgressBarAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                progressBar.setProgress(sumx * value + progress1);
            }
        });
        mProgressBarAnimator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                progressBar.setProgress(progress);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        if (listener != null) {
            mProgressBarAnimator.setStateChangedListener(listener);
        }
        progressBar.setMarkerProgress(progress);
        mProgressBarAnimator.start();
    }

    /**
     * generates random colors for the ProgressBar
     */
    protected void switchColor() {
        SecureRandom random = new SecureRandom();
        int randomColor = Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256));
        mHoloCircularProgressBar.setProgressColor(randomColor);

        randomColor = Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256));
        mHoloCircularProgressBar.setProgressBackgroundColor(randomColor);
    }

    /**
     * On active
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * On foreground *
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}

